<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Albums extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'albums';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['artist_id', 'name', 'year'];

    /**
     * Get artists relationship
     */
    public function artist()
    {
        return $this->hasOne('App\Artists', 'id', 'artist_id');
    }
}
